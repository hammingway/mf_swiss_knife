# -*- coding: utf-8 -*-

import sys

reload(sys)
import locale

sys.setdefaultencoding(locale.getpreferredencoding())

import logging
import logging.handlers
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('hexporter')
logger.setLevel(logging.INFO)

handler = logging.handlers.RotatingFileHandler(
    os.path.expanduser('~/%s' % 'hexporter.log'), maxBytes=1024 * 256, backupCount=2)
handler.setFormatter(logging.Formatter(u'%(levelname)-8s | %(asctime)s | %(message)s'))

logger.addHandler(handler)

from camelot.core.conf import settings, SimpleSettings
from camelot.core.utils import ugettext as _
from camelot.admin.action.application import Application
from hexporter.view import setup_views


class MyApplication(Application):

    def model_run(self, model_context):
        from camelot.core.conf import settings
        from camelot.core.utils import load_translations
        from camelot.view import action_steps
        from hexporter.importer import ImportXml
        from hexporter.dbaseio import ImportDBF

        yield action_steps.UpdateProgress(1, 6, _('Setup database'))
        settings.setup_model()
        yield action_steps.UpdateProgress(2, 6, _('Load translations'))
        load_translations()
        yield action_steps.UpdateProgress(3, 6, _('Install translator'))
        yield action_steps.InstallTranslator(model_context.admin)
        yield action_steps.UpdateProgress(4, 6, _('Load references'))
        yield ImportXml()
        yield action_steps.UpdateProgress(5, 6, _('Create main window'))
        yield action_steps.MainWindow(self.application_admin)
        home_actions = self.application_admin.get_actions()
        yield action_steps.ActionView(_('Home'), home_actions)
        #yield ImportDBF()


# begin custom settings
class MySettings(SimpleSettings):

    def setup_model(self):
        """This function will be called at application startup, it is used to 
        setup the model"""
        from sqlalchemy.orm import configure_mappers
        from camelot.core.sql import metadata
        from camelot.core.orm import setup_all
        from hexporter.constant import update_fixtures

        metadata.bind = self.ENGINE()
        import camelot.model.authentication
        import camelot.model.i18n
        import camelot.model.fixture
        import camelot.model.memento
        import hexporter.model

        setup_all()
        metadata.create_all()
        update_fixtures()
        setup_views()

    def ENGINE(self):
        from sqlalchemy import create_engine

        return create_engine('sqlite:///:memory:')


my_settings = MySettings('CRB', 'hexporter')
settings.append(my_settings)
# end custom settings


def start_application():
    from camelot.view.main import main_action
    from hexporter.application_admin import MyApplicationAdmin
    main_action(MyApplication(MyApplicationAdmin()))


if __name__ == '__main__':
    start_application()
