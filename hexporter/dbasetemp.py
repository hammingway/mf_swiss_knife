from model import Check, TempCheck, Passport, TempPassport, MedicalCare, TempMedicalCare, Notification, TempNotification
from sqlalchemy.sql import select, delete, join, func, and_
from utils import fields_from_list


def process_temporaries(session):
    q_g = select([TempCheck.nom_ch.label('g_nom'),
                  func.max(TempCheck.date_fil).label('g_date'),
                  TempCheck.date_per.label('g_period'),
                  TempCheck.type.label('g_type')],
                 group_by=[TempCheck.type,
                           TempCheck.date_per,
                           TempCheck.nom_ch]).alias()

    q_j = join(TempCheck, q_g, and_(TempCheck.nom_ch == q_g.c.g_nom, TempCheck.date_fil == q_g.c.g_date,
                                    TempCheck.date_per == q_g.c.g_period, TempCheck.type == q_g.c.g_type)) \
        .select().alias()

    attrs = ['lpu_k1', 'lpu_k2', 'lpu_name', 'smo_name', 'ur_lpu', 'vid_med', 'inn', 'date_ch', 'nom_ch', 'ed_izm',
             'kolvo', 'summ_ch', 'date_per', 'fail_ch', 'kod_dev'] + ['smo_kode', 'type']

    q = select(fields_from_list(q_j, attrs)).alias()

    q = Check.table.insert(inline=True).from_select(attrs, q)
    session.execute(q)

    q_j = join(q_g, TempNotification, and_(q_g.c.g_type.in_((u'3', u'4', u'5')),
                                           q_g.c.g_type == TempNotification.type,
                                           q_g.c.g_nom == TempNotification.nom_ch,
                                           q_g.c.g_period == TempNotification.date_per,
                                           q_g.c.g_date == TempNotification.date_fil)).select().alias()

    attrs = ['npr_e', 'npr', 'npr_s', 'fact_dn', 'kol_ye', 'vmedpom', 'rezultat', 'err_code', 'idprof', 'regn_pac',
             'summa', 'sum_otkl', 'ncard', 'smo', 'lpu', 'kol_usl', 'rstamp', 'prikrep', 'lpu_prik', 'dso', 'dbeg',
             'dend', 'kod_usl'] + ['nom_ch', 'date_per', 'type']

    q = select(fields_from_list(q_j, attrs)).alias()

    q = Notification.table.insert(inline=True).from_select(attrs, q)
    session.execute(q)

    q_j = join(q_g, TempPassport, and_(q_g.c.g_type == u'6',
                                       q_g.c.g_type == TempPassport.type,
                                       q_g.c.g_nom == TempPassport.nom_ch,
                                       q_g.c.g_period == TempPassport.date_per,
                                       q_g.c.g_date == TempPassport.date_fil)).select().alias()

    attrs = ['npr', 'n_obllpu', 'regn_pac', 'ncard', 'fam', 'im', 'otch', 'fam_r', 'im_r', 'otch_r', 'pol', 'dr',
             'spolis', 'npolis', 'doc_tip', 'doc_ser', 'doc_ser1', 'doc_num', 'doc_num1', 'rabota', 'adr_rab',
             'adr_reg', 'adr_raion', 'adr_punkt', 'adr_ulica', 'adr_dom', 'adr_korp', 'adr_kvart', 'd_type', 'file_vid',
             'ss', 'pol_tip', 'oms_num', 'tmp_num', 'vnov_d', 'okatog', 'okatop'] + ['nom_ch', 'date_per', 'smo_kode',
                                                                                     'type']

    q = select(fields_from_list(q_j, attrs), distinct=True).alias()

    q = Passport.table.insert(inline=True).from_select(attrs, q)
    session.execute(q)

    q_j = join(q_g, TempMedicalCare, and_(q_g.c.g_type == u'6',
                                          q_g.c.g_type == TempMedicalCare.type,
                                          q_g.c.g_nom == TempMedicalCare.nom_ch,
                                          q_g.c.g_period == TempMedicalCare.date_per,
                                          q_g.c.g_date == TempMedicalCare.date_fil)).select().alias()

    attrs = ['npr_s', 'npr', 'vmedpom', 'c_pos_st', 'kod_vrl', 'idprof', 'idotd', 'dso', 'dbeg', 'dend', 'tarif',
             'fact_dn', 'kol_ye', 'dopl', 'summa', 'sts', 'otcl_dn', 'otcl_pro', 'ishod', 'regn_pac', 'datakek', 'nkek',
             'kod_usl', 'tip_file', 'kol_usl', 'dlistin', 'dlistout', 'list_num', 'ds_s', 'date_ns', 'kod_oper', 'ds_o',
             'kyc', 'kckp', 'ky', 'hvid', 'hmet', 'prikrep', 'profil', 'osn_kskp', 'gs', 'kod_mo', 'koz', 'usl_ok',
             'npr_mo', 'det', 'ds_p', 'vnov_m', 'rslt', 'prvs', 'ishod_f', 'idsp', 'for_pom', 'diagnoz', 'priz_zabol',
             'mrs', 'ds_z', 'podr', 'inv', 'rean_dn', 'smp_vyz', 'smp_prib'] \
            + ['lpu', 'nom_ch', 'date_per', 'smo_kode', 'type']

    q = select(fields_from_list(q_j, attrs)).alias()

    q = MedicalCare.table.insert(inline=True).from_select(attrs, q)
    session.execute(q)

    q = delete(TempCheck.table)
    session.execute(q)
    q = delete(TempPassport.table)
    session.execute(q)
    q = delete(TempMedicalCare.table)
    session.execute(q)
    q = delete(TempNotification.table)
    session.execute(q)
