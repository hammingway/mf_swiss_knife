from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.admin.entity_admin import EntityAdmin
from camelot.core.orm.options import using_options
from camelot.admin import table
from model import CareTypes, RejectionDescr, MedFoundation
from operator import itemgetter
from utils import log_exceptions


@log_exceptions
def update_fixtures():
    from camelot.model.fixture import Fixture
    from references import care_types, rejection_codes, med_founds

    def parse(ds):
        for rl in ds.split('\n'):
            l = rl.strip()
            if not l:
                continue
            yield tuple(p.strip() for p in l.split('|'))

    for vmedpom, short, full in parse(care_types):
        vmp = vmedpom.lower()
        Fixture.insert_or_update_fixture(
            CareTypes,
            fixture_key=vmp,
            values=dict(vmedpom=vmp, short=short, full=full)
        )

    title = ''
    for code, descr in sorted(((c.strip('.'), d) for c, d in parse(rejection_codes)), key=itemgetter(0)):
        if len(code) == 3:
            title = '%s ' % descr
            Fixture.insert_or_update_fixture(
                RejectionDescr,
                fixture_key=code,
                values=dict(code=code, descr=descr)
            )
        else:
            Fixture.insert_or_update_fixture(
                RejectionDescr,
                fixture_key=code,
                values=dict(code=code, descr=('%s%s' % (title, descr)).strip())
            )

    for no, jur, parent, sub, lpu, foms, dcode, name, distr in parse(med_founds):
        Fixture.insert_or_update_fixture(
            MedFoundation,
            fixture_key=repr(no),
            values=dict(no=int(no),
                        jur=int(jur) if jur else None,
                        parent=int(parent) if parent else None,
                        lpu=lpu.upper(),
                        foms=int(foms),
                        dcode=int(dcode),
                        name=name,
                        distr=distr)
        )


class UpdateFixtures(Action):
    verbose_name = _('Refresh references')

    def model_run(self, model_context):
        from camelot.view.action_steps import Refresh

        update_fixtures()
        yield Refresh()