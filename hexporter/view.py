# -*- coding: utf-8 -*-

from camelot.core.utils import ugettext_lazy as _
from camelot.admin.entity_admin import EntityAdmin
from camelot.admin import table


class Completed(object):
    class Admin(EntityAdmin):
        verbose_name = _('Complete')
        verbose_name_plural = _('Complete')
        list_display = table.Table(['foms', 'ss', 'polis', 'ishod', 'dso', 'dn', 'dbeg', 'dend'])


class Errors(object):
    class Admin(EntityAdmin):
        verbose_name = _('Has errors')
        verbose_name_plural = _('Has errors')
        list_display = table.Table(['ncard', 'fam', 'im', 'otch', 'fam_r', 'im_r', 'otch_r', 'pol', 'dr', 'spolis',
                                    'npolis', 'ss', 'pol_tip', 'oms_num', 'tmp_num',
                                    'vmedpom', 'dso', 'rslt'])


class Period(object):
    class Admin(EntityAdmin):
        verbose_name = _('Period')
        verbose_name_plural = _('Period')
        list_display = table.Table(['begin', 'end'])


def setup_views():
    from sqlalchemy.sql import select, outerjoin, join, func, literal, and_, or_, case
    from sqlalchemy.orm import mapper, Session

    from model import Check, Passport, MedicalCare, MedFoundation, DisprsltEntity

    q = select(
        [Passport.id.label('p_id'),
         MedicalCare.id.label('mc_id'),
         MedFoundation.foms,
         Passport.ss,
         case(
             [(Passport.pol_tip == 3, Passport.oms_num)]
         ).label('polis'),
         DisprsltEntity.IDDR.label('ishod'),
         MedicalCare.dso,
         case(
             [(MedicalCare.rslt == 317, u'нет')],
             else_=u'да'
         ).label('dn'),
         MedicalCare.dbeg,
         MedicalCare.dend
         ],
        whereclause=and_(
            func.upper(MedicalCare.vmedpom).in_(['R', 'S', 'T', 'V']),
            or_(
                func.trim(Passport.ss) != '',
                Passport.pol_tip == 3
            )
        ),
        distinct=True) \
        .select_from(
        Passport.table.join(
            MedicalCare,
            and_(
                Passport.npr == MedicalCare.npr,
                Passport.nom_ch == MedicalCare.nom_ch
            )
        ).join(
            DisprsltEntity,
            MedicalCare.rslt == DisprsltEntity.RSLT
        ).join(
            Check,
            MedicalCare.nom_ch == Check.nom_ch
        ).join(
            MedFoundation,
            Check.lpu_k2 == MedFoundation.lpu
        )
    ).alias()

    mapper(Completed, q)

    q = select(
        [Passport.id.label('p_id'),
         MedicalCare.id.label('mc_id'),
         Passport.ncard,
         Passport.fam,
         Passport.im,
         Passport.otch,
         Passport.fam_r,
         Passport.im_r,
         Passport.otch_r,
         Passport.pol,
         Passport.dr,
         Passport.spolis,
         Passport.npolis,
         Passport.ss,
         Passport.pol_tip,
         Passport.oms_num,
         Passport.tmp_num,
         MedicalCare.vmedpom,
         MedicalCare.dso,
         MedicalCare.rslt],
        whereclause=or_(
            func.upper(MedicalCare.vmedpom).notin_(['R', 'S', 'T', 'V']),
            and_(
                func.trim(Passport.ss) == '',
                Passport.pol_tip != 3
            ),
            MedicalCare.rslt.notin_(select([DisprsltEntity.RSLT]))
        ),
        distinct=True) \
        .select_from(
        Passport.table.join(
            MedicalCare,
            and_(
                Passport.npr == MedicalCare.npr,
                Passport.nom_ch == MedicalCare.nom_ch
            )
        )
    ).alias()

    mapper(Errors, q)

    q = select(
        [func.min(MedicalCare.dend).label('begin'),
         func.max(MedicalCare.dend).label('end')],
    ).alias()

    mapper(Period, q, primary_key=q.c.begin)
