from collections import OrderedDict
from sqlalchemy.schema import Column
from sqlalchemy import BigInteger, Unicode, Date, DateTime, Numeric, Index
from camelot.core.utils import ugettext_lazy as _

from camelot.admin.entity_admin import EntityAdmin
from camelot.core.orm import Entity
from specs import specs_all, iter_specs
import sys
import camelot.types


class Check(Entity):
    __tablename__ = 'checks'
    lpu_k1 = Column(Unicode(2))
    lpu_k2 = Column(Unicode(2), index=True)
    lpu_name = Column(Unicode(100))
    smo_name = Column(Unicode(100))
    ur_lpu = Column(BigInteger)
    vid_med = Column(Unicode(2))
    inn = Column(BigInteger)
    date_ch = Column(Date)
    nom_ch = Column(Unicode(30), index=True)
    ed_izm = Column(Unicode(10))
    kolvo = Column(Numeric(precision=12, scale=2))
    summ_ch = Column(Numeric(precision=12, scale=2))
    date_per = Column(Date)
    fail_ch = Column(Unicode(12))
    kod_dev = Column(BigInteger)

    smo_kode = Column(Unicode(3), index=True)
    type = Column(Unicode(1), index=True)

    class Admin(EntityAdmin):
        verbose_name = _('Check')
        verbose_name_plural = _('Checks')
        list_display = ['lpu_k1', 'lpu_k2', 'lpu_name', 'smo_name', 'ur_lpu', 'vid_med', 'inn', 'date_ch', 'nom_ch',
                        'ed_izm', 'kolvo', 'summ_ch', 'date_per', 'fail_ch', 'kod_dev']


class TempCheck(Entity):
    __tablename__ = 'temp_checks'
    __table_args__ = {'prefixes': ['TEMPORARY']}
    lpu_k1 = Column(Unicode(2))
    lpu_k2 = Column(Unicode(2))
    lpu_name = Column(Unicode(100))
    smo_name = Column(Unicode(100))
    ur_lpu = Column(BigInteger)
    vid_med = Column(Unicode(2))
    inn = Column(BigInteger)
    date_ch = Column(Date)
    nom_ch = Column(Unicode(30))
    ed_izm = Column(Unicode(10))
    kolvo = Column(Numeric(precision=12, scale=2))
    summ_ch = Column(Numeric(precision=12, scale=2))
    date_per = Column(Date)
    fail_ch = Column(Unicode(12))
    kod_dev = Column(BigInteger)

    date_fil = Column(DateTime())
    smo_kode = Column(Unicode(3))
    type = Column(Unicode(1))


class Passport(Entity):
    __tablename__ = 'passports'
    npr = Column(BigInteger, index=True)
    n_obllpu = Column(BigInteger)
    regn_pac = Column(BigInteger)
    ncard = Column(Unicode(17))
    fam = Column(Unicode(40))
    im = Column(Unicode(40))
    otch = Column(Unicode(40))
    fam_r = Column(Unicode(40))
    im_r = Column(Unicode(40))
    otch_r = Column(Unicode(40))
    pol = Column(Unicode(1))
    dr = Column(Date)
    spolis = Column(Unicode(5))
    npolis = Column(BigInteger)
    doc_tip = Column(BigInteger)
    doc_ser = Column(Unicode(10))
    doc_ser1 = Column(Unicode(5))
    doc_num = Column(Unicode(10))
    doc_num1 = Column(Unicode(7))
    rabota = Column(Unicode(95))
    adr_rab = Column(BigInteger)
    adr_reg = Column(BigInteger)
    adr_raion = Column(BigInteger)
    adr_punkt = Column(Unicode(63))
    adr_ulica = Column(Unicode(63))
    adr_dom = Column(Unicode(10))
    adr_korp = Column(Unicode(2))
    adr_kvart = Column(Unicode(5))
    d_type = Column(Unicode(4))
    file_vid = Column(Unicode(1))
    ss = Column(Unicode(14))
    pol_tip = Column(BigInteger)
    oms_num = Column(Unicode(16))
    tmp_num = Column(Unicode(9))
    vnov_d = Column(BigInteger)
    okatog = Column(Unicode(11))
    okatop = Column(Unicode(11))

    nom_ch = Column(Unicode(30), index=True)
    date_per = Column(Date())
    smo_kode = Column(Unicode(3))
    type = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name = _('Passport')
        verbose_name_plural = _('Passports')
        list_display = ['npr', 'n_obllpu', 'regn_pac', 'ncard', 'fam', 'im', 'otch', 'fam_r', 'im_r', 'otch_r', 'pol',
                        'dr', 'spolis', 'npolis', 'doc_tip', 'doc_ser', 'doc_ser1', 'doc_num', 'doc_num1', 'rabota',
                        'adr_rab', 'adr_reg', 'adr_raion', 'adr_punkt', 'adr_ulica', 'adr_dom', 'adr_korp', 'adr_kvart',
                        'd_type', 'file_vid', 'ss', 'pol_tip', 'oms_num', 'tmp_num', 'vnov_d', 'okatog', 'okatop']


class TempPassport(Entity):
    __tablename__ = 'temp_passports'
    __table_args__ = {'prefixes': ['TEMPORARY']}
    npr = Column(BigInteger)
    n_obllpu = Column(BigInteger)
    regn_pac = Column(BigInteger)
    ncard = Column(Unicode(17))
    fam = Column(Unicode(40))
    im = Column(Unicode(40))
    otch = Column(Unicode(40))
    fam_r = Column(Unicode(40))
    im_r = Column(Unicode(40))
    otch_r = Column(Unicode(40))
    pol = Column(Unicode(1))
    dr = Column(Date)
    spolis = Column(Unicode(5))
    npolis = Column(BigInteger)
    doc_tip = Column(BigInteger)
    doc_ser = Column(Unicode(10))
    doc_ser1 = Column(Unicode(5))
    doc_num = Column(Unicode(10))
    doc_num1 = Column(Unicode(7))
    rabota = Column(Unicode(95))
    adr_rab = Column(BigInteger)
    adr_reg = Column(BigInteger)
    adr_raion = Column(BigInteger)
    adr_punkt = Column(Unicode(63))
    adr_ulica = Column(Unicode(63))
    adr_dom = Column(Unicode(10))
    adr_korp = Column(Unicode(2))
    adr_kvart = Column(Unicode(5))
    d_type = Column(Unicode(4))
    file_vid = Column(Unicode(1))
    ss = Column(Unicode(14))
    pol_tip = Column(BigInteger)
    oms_num = Column(Unicode(16))
    tmp_num = Column(Unicode(9))
    vnov_d = Column(BigInteger)
    okatog = Column(Unicode(11))
    okatop = Column(Unicode(11))

    nom_ch = Column(Unicode(30))
    date_per = Column(Date())
    date_fil = Column(DateTime())
    smo_kode = Column(Unicode(3))
    type = Column(Unicode(1))


class MedicalCare(Entity):
    __tablename__ = 'cares'
    npr_s = Column(BigInteger)
    npr = Column(BigInteger, index=True)
    vmedpom = Column(Unicode(1))
    c_pos_st = Column(BigInteger)
    kod_vrl = Column(BigInteger)
    idprof = Column(BigInteger)
    idotd = Column(Unicode(4))
    dso = Column(Unicode(6))
    dbeg = Column(Date)
    dend = Column(Date)
    tarif = Column(Numeric(precision=10, scale=2))
    fact_dn = Column(BigInteger)
    kol_ye = Column(Numeric(precision=05, scale=2))
    dopl = Column(Numeric(precision=12, scale=2))
    summa = Column(Numeric(precision=12, scale=2))
    sts = Column(BigInteger)
    otcl_dn = Column(BigInteger)
    otcl_pro = Column(Numeric(precision=6, scale=1))
    ishod = Column(BigInteger)
    regn_pac = Column(BigInteger)
    datakek = Column(Date)
    nkek = Column(Unicode(6))
    kod_usl = Column(Unicode(10))
    tip_file = Column(Unicode(1))
    kol_usl = Column(BigInteger)
    dlistin = Column(Date)
    dlistout = Column(Date)
    list_num = Column(Unicode(12))
    ds_s = Column(Unicode(7))
    date_ns = Column(Date)
    kod_oper = Column(Unicode(18))
    ds_o = Column(Unicode(7))
    kyc = Column(Numeric(precision=5, scale=2))
    kckp = Column(Numeric(precision=5, scale=2))
    ky = Column(Numeric(precision=5, scale=3))
    hvid = Column(Unicode(12))
    hmet = Column(BigInteger)
    prikrep = Column(Unicode(1))
    profil = Column(BigInteger)
    osn_kskp = Column(Unicode(15))
    gs = Column(Unicode(1))
    kod_mo = Column(Unicode(2))
    koz = Column(Numeric(precision=5, scale=2))
    usl_ok = Column(BigInteger)
    npr_mo = Column(Unicode(6))
    det = Column(BigInteger)
    ds_p = Column(Unicode(7))
    vnov_m = Column(BigInteger)
    rslt = Column(BigInteger)
    prvs = Column(Unicode(9))
    ishod_f = Column(BigInteger)
    idsp = Column(BigInteger)
    for_pom = Column(BigInteger)
    diagnoz = Column(BigInteger)
    priz_zabol = Column(BigInteger)
    mrs = Column(Unicode(8))
    ds_z = Column(Unicode(6))
    podr = Column(BigInteger)
    inv = Column(Unicode(1))
    rean_dn = Column(BigInteger)
    smp_vyz = Column(Unicode(16))
    smp_prib = Column(Unicode(16))

    lpu = Column(Unicode(2))
    nom_ch = Column(Unicode(30), index=True)
    date_per = Column(Date())
    smo_kode = Column(Unicode(3))
    type = Column(Unicode(1))

    class Admin(EntityAdmin):
        verbose_name = _('Medical care')
        verbose_name_plural = _('Medical cares')
        list_display = ['npr_s', 'npr', 'vmedpom', 'c_pos_st', 'kod_vrl', 'idprof', 'idotd', 'dso', 'dbeg', 'dend',
                        'tarif', 'fact_dn', 'kol_ye', 'dopl', 'summa', 'sts', 'otcl_dn', 'otcl_pro', 'ishod',
                        'regn_pac', 'datakek', 'nkek', 'kod_usl', 'tip_file', 'kol_usl', 'dlistin', 'dlistout',
                        'list_num', 'ds_s', 'date_ns', 'kod_oper', 'ds_o', 'kyc', 'kckp', 'ky', 'hvid', 'hmet',
                        'prikrep', 'profil', 'osn_kskp', 'gs', 'kod_mo', 'koz', 'usl_ok', 'npr_mo', 'det', 'ds_p',
                        'vnov_m', 'rslt', 'prvs', 'ishod_f', 'idsp', 'for_pom', 'diagnoz', 'priz_zabol', 'mrs', 'ds_z',
                        'podr', 'inv', 'rean_dn', 'smp_vyz', 'smp_prib']


class TempMedicalCare(Entity):
    __tablename__ = 'temp_cares'
    __table_args__ = {'prefixes': ['TEMPORARY']}
    npr_s = Column(BigInteger)
    npr = Column(BigInteger)
    vmedpom = Column(Unicode(1))
    c_pos_st = Column(BigInteger)
    kod_vrl = Column(BigInteger)
    idprof = Column(BigInteger)
    idotd = Column(Unicode(4))
    dso = Column(Unicode(6))
    dbeg = Column(Date)
    dend = Column(Date)
    tarif = Column(Numeric(precision=10, scale=2))
    fact_dn = Column(BigInteger)
    kol_ye = Column(Numeric(precision=05, scale=2))
    dopl = Column(Numeric(precision=12, scale=2))
    summa = Column(Numeric(precision=12, scale=2))
    sts = Column(BigInteger)
    otcl_dn = Column(BigInteger)
    otcl_pro = Column(Numeric(precision=6, scale=1))
    ishod = Column(BigInteger)
    regn_pac = Column(BigInteger)
    datakek = Column(Date)
    nkek = Column(Unicode(6))
    kod_usl = Column(Unicode(10))
    tip_file = Column(Unicode(1))
    kol_usl = Column(BigInteger)
    dlistin = Column(Date)
    dlistout = Column(Date)
    list_num = Column(Unicode(12))
    ds_s = Column(Unicode(7))
    date_ns = Column(Date)
    kod_oper = Column(Unicode(18))
    ds_o = Column(Unicode(7))
    kyc = Column(Numeric(precision=5, scale=2))
    kckp = Column(Numeric(precision=5, scale=2))
    ky = Column(Numeric(precision=5, scale=3))
    hvid = Column(Unicode(12))
    hmet = Column(BigInteger)
    prikrep = Column(Unicode(1))
    profil = Column(BigInteger)
    osn_kskp = Column(Unicode(15))
    gs = Column(Unicode(1))
    kod_mo = Column(Unicode(2))
    koz = Column(Numeric(precision=5, scale=2))
    usl_ok = Column(BigInteger)
    npr_mo = Column(Unicode(6))
    det = Column(BigInteger)
    ds_p = Column(Unicode(7))
    vnov_m = Column(BigInteger)
    rslt = Column(BigInteger)
    prvs = Column(Unicode(9))
    ishod_f = Column(BigInteger)
    idsp = Column(BigInteger)
    for_pom = Column(BigInteger)
    diagnoz = Column(BigInteger)
    priz_zabol = Column(BigInteger)
    mrs = Column(Unicode(8))
    ds_z = Column(Unicode(6))
    podr = Column(BigInteger)
    inv = Column(Unicode(1))
    rean_dn = Column(BigInteger)
    smp_vyz = Column(Unicode(16))
    smp_prib = Column(Unicode(16))

    lpu = Column(Unicode(2))
    nom_ch = Column(Unicode(30))
    date_per = Column(Date())
    date_fil = Column(DateTime())
    smo_kode = Column(Unicode(3))
    type = Column(Unicode(1))


class Notification(Entity):
    __tablename__ = 'notifications'
    npr_e = Column(BigInteger)
    npr = Column(BigInteger, index=True)
    npr_s = Column(BigInteger)
    fact_dn = Column(BigInteger)
    kol_ye = Column(Numeric(precision=5, scale=2))
    vmedpom = Column(Unicode(1))
    rezultat = Column(BigInteger)
    err_code = Column(Unicode(10))
    idprof = Column(BigInteger)
    regn_pac = Column(BigInteger)
    summa = Column(Numeric(precision=12, scale=2))
    sum_otkl = Column(Numeric(precision=12, scale=2))
    ncard = Column(Unicode(17))
    smo = Column(BigInteger)
    lpu = Column(Unicode(2))
    kol_usl = Column(BigInteger)
    rstamp = Column(Unicode(8))
    prikrep = Column(Unicode(1))
    lpu_prik = Column(Unicode(2))
    dso = Column(Unicode(6))
    dbeg = Column(Date)
    dend = Column(Date)
    kod_usl = Column(Unicode(10))

    nom_ch = Column(Unicode(30), index=True)
    date_per = Column(Date(), index=True)
    type = Column(Unicode(1), index=True)

    class Admin(EntityAdmin):
        verbose_name = _('Notification')
        verbose_name_plural = _('Notifications')
        list_display = ['npr_e', 'npr', 'npr_s', 'fact_dn', 'kol_ye', 'vmedpom', 'rezultat', 'err_code', 'idprof',
                        'regn_pac', 'summa', 'sum_otkl', 'ncard', 'smo', 'lpu', 'kol_usl', 'rstamp', 'prikrep',
                        'lpu_prik', 'dso', 'dbeg', 'dend', 'kod_usl']


class TempNotification(Entity):
    __tablename__ = 'temp_notifications'
    __table_args__ = {'prefixes': ['TEMPORARY']}
    npr_e = Column(BigInteger)
    npr = Column(BigInteger)
    npr_s = Column(BigInteger)
    fact_dn = Column(BigInteger)
    kol_ye = Column(Numeric(precision=5, scale=2))
    vmedpom = Column(Unicode(1))
    rezultat = Column(BigInteger)
    err_code = Column(Unicode(10))
    idprof = Column(BigInteger)
    regn_pac = Column(BigInteger)
    summa = Column(Numeric(precision=12, scale=2))
    sum_otkl = Column(Numeric(precision=12, scale=2))
    ncard = Column(Unicode(17))
    smo = Column(BigInteger)
    lpu = Column(Unicode(2))
    kol_usl = Column(BigInteger)
    rstamp = Column(Unicode(8))
    prikrep = Column(Unicode(1))
    lpu_prik = Column(Unicode(2))
    dso = Column(Unicode(6))
    dbeg = Column(Date)
    dend = Column(Date)
    kod_usl = Column(Unicode(10))

    nom_ch = Column(Unicode(30))
    date_per = Column(Date)
    date_fil = Column(DateTime())
    type = Column(Unicode(1))


class Statkart(Entity):
    __tablename__ = 'stat_card'
    fam = Column(Unicode(40), index=True)
    im = Column(Unicode(40), index=True)
    ot = Column(Unicode(40), index=True)
    dat_rojd = Column(Date, index=True)
    dat_post = Column(Date)
    dat_vyp = Column(Date)

    class Admin(EntityAdmin):
        verbose_name = _('Statistical kards')
        verbose_name_plural = _('Statistical kards')
        list_display = ['fam', 'im', 'ot', 'dat_rojd', 'dat_post', 'dat_vyp']


class Hospitalized(Entity):
    __tablename__ = 'hospitalized'
    id = Column(BigInteger, primary_key=True)
    nkard = Column(BigInteger)
    fam = Column(Unicode(40), index=True)
    im = Column(Unicode(40), index=True)
    ot = Column(Unicode(40), index=True)
    dr = Column(Date, index=True)
    spol = Column(Unicode(5))
    npol = Column(Unicode(16))
    strah_comp = Column(Unicode(20))
    mkb = Column(Unicode(6))
    hosp_date = Column(Date)
    prof_bed = Column(BigInteger)
    nap_date = Column(Date)
    otd = Column(Unicode(20))
    predp_vip_date = Column(Date)

    class Admin(EntityAdmin):
        verbose_name = _('Hospitalized patients')
        verbose_name_plural = _('Hospitalized patients')
        list_display = ['id', 'fam', 'im', 'ot', 'dr', 'spol', 'npol', 'strah_comp', 'mkb', 'hosp_date', 'prof_bed',
                        'nap_date', 'otd', 'predp_vip_date']


# References models

class CareTypes(Entity):
    __tablename__ = 'care_types'
    vmedpom = Column(Unicode(2), index=True, unique=True)
    short = Column(Unicode(64))
    full = Column(camelot.types.RichText)

    class Admin(EntityAdmin):
        verbose_name = _('Care type')
        verbose_name_plural = _('Care types')
        list_display = ['id', 'vmedpom', 'short', 'full']


class RejectionDescr(Entity):
    __tablename__ = 'rejection_descr'
    code = Column(Unicode(10), index=True, unique=True)
    descr = Column(camelot.types.RichText)

    class Admin(EntityAdmin):
        verbose_name = _('Rejection description')
        verbose_name_plural = _('Rejection descriptions')
        list_display = ['id', 'code', 'descr']


class MedFoundation(Entity):
    __tablename__ = 'medical_found'
    no = Column(BigInteger(), unique=True)
    jur = Column(BigInteger())
    parent = Column(BigInteger())
    lpu = Column(Unicode(2), index=True, unique=True)
    foms = Column(BigInteger(), index=True)
    dcode = Column(BigInteger())
    name = Column(camelot.types.RichText)
    distr = Column(Unicode(30))

    class Admin(EntityAdmin):
        verbose_name = _('Medical foundation')
        verbose_name_plural = _('Medical foundations')
        list_display = ['id', 'no', 'jur', 'parent', 'lpu', 'foms', 'dcode', 'name', 'distr']


################################################################################

autogenerated = OrderedDict(iter_specs(specs_all))
sys.modules[__name__].__dict__.update(autogenerated)

Index('ix_%s_IDDR' % DisprEntity.__tablename__, DisprEntity.IDDR)
Index('ix_%s_IDDR' % DisprsltEntity.__tablename__, DisprsltEntity.IDDR)
Index('ix_%s_RSLT' % DisprsltEntity.__tablename__, DisprsltEntity.RSLT)
Index('ix_%s_vmedpom' % S007Entity.__tablename__, S007Entity.vmedpom)
