# -*- coding: utf-8 -*-

# import sys
import urllib2
import re
import logging
from datetime import datetime
from lxml.etree import Element, SubElement, ElementTree, dump, fromstring
from cStringIO import StringIO
from camelot.admin.action import Action, ActionStep
from camelot.core.utils import ugettext_lazy as _
from camelot.core.qt import QtWidgets, QtGui
from camelot.view.art import Icon
from utils import RecordProxy, read_dbf, log_exceptions
from model import Hospitalized, Statkart
from camelot.admin.object_admin import ObjectAdmin
from camelot.view.controls import delegates


class ProtocolException(Exception):
    pass


class OperationException(Exception):
    pass


class Hospitalize(object):
    _HOST = 'http://172.18.1.8:8000/IPORTAL_XML_RPC/default/call/xmlrpc'
    HOST_TEST = 'http://172.18.1.8:8000/IPORTAL_XML_RPC_TEST/default/call/xmlrpc'
    HOST = _HOST
    RDATE = re.compile('(\d{2})\.(\d{2})\.(\d{4})')

    def __init__(self, login, password):
        self.login = login
        self.password = password

    def _value(self, el):
        if el.text is None or el.tag == 'nil':
            return None
        if el.tag == 'string':
            text = el.text.strip()
            if self.RDATE.match(text) is not None:
                return datetime.strptime(text, '%d.%m.%Y').date()
            else:
                return text
        elif el.tag == 'int':
            return int(el.text)
        else:
            raise ProtocolException('bad value type')

    def get_hospitalized(self):
        elr = self._method('AIS_hospitaliz_get_hospitalized')
        self._prologue(elr)
        return elr

    def get_hospitalized_response(self, respstr):
        bvals = self._response_status(respstr)

        def process(data):
            for rec in data.xpath('array/data/*'):
                datarec = {}
                for i in rec.xpath('struct/*'):
                    datarec[i.find('name').text] = self._value(i.xpath('value/*')[0])
                yield datarec

        return list(process(bvals[2]))

    def out(self, id_, mo=None):
        elr = self._method('AIS_hospitaliz_out')
        el = self._prologue(elr)
        self._member(el, 'ID', str(id_))
        if mo:
            self._member(el, 'MO', str(mo))
        return elr

    def out_response(self, respstr):
        self._response_status(respstr)

    def post(self, el):
        string = StringIO()
        try:
            ElementTree(el).write(string, xml_declaration=True, encoding='utf-8')
            response = urllib2.urlopen(
                urllib2.Request(
                    self.HOST,
                    string.getvalue(),
                    {'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'})
            )
            reply = response.read()
        finally:
            string.close()
        return reply

    @staticmethod
    def _method(name):
        el = Element('methodCall')
        SubElement(el, 'methodName').text = name
        return el

    @staticmethod
    def _member(el, name, value):
        el = SubElement(el, 'member')
        SubElement(el, 'name').text = name
        SubElement(SubElement(el, 'value'), 'string').text = value

    def _prologue(self, el):
        el = SubElement(el, 'params')
        el = SubElement(el, 'param')
        el = SubElement(el, 'value')
        el = SubElement(el, 'struct')
        self._member(el, 'LOGIN', self.login)
        self._member(el, 'PASSWORD', self.password)
        return el

    def _response_status(self, respstr):
        et = fromstring(respstr)
        bvals = et.xpath('//methodResponse/params/param/value/array/data/*')
        try:
            ec, ed = (i.xpath('string')[0].text for i in bvals[:2])
        except Exception, e:
            raise ProtocolException(e)
        if ec != '0':
            raise OperationException('%s (%s)' % (ec, ed))
        return bvals


class Login(object):
    def __init__(self):
        self.login = None
        self.password = None

    class Admin(ObjectAdmin):
        list_display = ['login', 'password']
        field_attributes = {'login':
                                {'name': _('Login'),
                                 'delegate': delegates.PlainTextDelegate,
                                 'editable': True},
                            'password':
                                {'name': _('Password'),
                                 'delegate': delegates.PlainTextDelegate,
                                 'echo_mode': QtWidgets.QLineEdit.Password,
                                 'editable': True}}


class HospitalOut(Action):
    icon = Icon('tango/16x16/categories/applications-system.png')
    verbose_name = _('Do out')

    @log_exceptions
    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession,
                                               ChangeObject,
                                               MessageBox)
        from camelot.core.orm import Session
        from sqlalchemy.sql import select, delete, and_, or_

        select_statk = SelectFile(_('XBase Files (*.dbf;*.DBF)'))
        select_statk.single = True
        file_names = yield select_statk

        def clean_tables(session):
            lst = (delete(Hospitalized.table),
                   delete(Statkart.table))
            for i in lst:
                session.execute(i)

        session = model_context.session
        rejected, accepted = 0, 0
        with session.begin():
            yield UpdateProgress(0, 1, _('Preparation'))
            clean_tables(session)

            for i, rec in enumerate(read_dbf(file_names[0])):
                tbl = Statkart()
                rec.fill_model(tbl)
                tbl.fam = (rec.FAM or rec.FAM_R or '').upper()
                tbl.im = (rec.IM or rec.IM_R or '').upper()
                tbl.ot = (rec.OT or rec.OTCH_R or '').upper()
                if i % 200 == 0:
                    yield FlushSession(session)
                    yield UpdateProgress(rec.record__.index, rec.table__.recordCount, _('Loading .DBF'))
            yield FlushSession(session)

            yield UpdateProgress(0, 1, _('Data acquisition'))
            login_obj = Login()
            while True:
                yield ChangeObject(login_obj)
                hosp = Hospitalize(login_obj.login, login_obj.password)
                el = hosp.get_hospitalized()
                reply = hosp.post(el)
                try:
                    hospitalized = hosp.get_hospitalized_response(reply)
                except OperationException, e:
                    if e.message[0] == '1':
                        # Неверный логин или пароль
                        continue
                    else:
                        raise e
                break
            for i, rec in enumerate(hospitalized):
                tbl = Hospitalized()
                tbl.id = rec.get('ID')
                tbl.nkard = rec.get('NKARD')
                tbl.fam = rec['FAM'].upper()
                tbl.im = rec['IM'].upper()
                tbl.ot = (rec['OT'] or '').upper()
                tbl.dr = rec['DR']
                tbl.spol = rec.get('SPOL')
                tbl.npol = rec.get('NPOL')
                tbl.strah_comp = rec.get('STRAH_COMP')
                tbl.mkb = rec.get('MKB')
                tbl.hosp_date = rec.get('HOSP_DATE')
                tbl.prof_bed = rec.get('PROF_BED')
                tbl.nap_date = rec.get('NAP_DATE')
                tbl.otd = rec.get('OTD')
                tbl.predp_vip_date = rec.get('PREDP_VIP_DATE')
                if i % 200 == 0:
                    yield FlushSession(session)
                    yield UpdateProgress(i, len(hospitalized), _('Data acquisition'))
            yield FlushSession(session)
        yield Refresh()

        q = select(
            [Hospitalized.id],
            whereclause=and_(
                Hospitalized.hosp_date >= Statkart.dat_post,
                or_(
                    Hospitalized.hosp_date <= Statkart.dat_vyp,
                    Statkart.dat_vyp.is_(None)
                )
            ),
            distinct=True
        ).select_from(
            Hospitalized.table.join(
                Statkart,
                and_(
                    Hospitalized.fam == Statkart.fam,
                    Hospitalized.im == Statkart.im,
                    Hospitalized.ot == Statkart.ot,
                    Hospitalized.dr == Statkart.dat_rojd
                )
            )
        ).alias()
        yield UpdateProgress(0, 1, _('Doing out'))
        patients = list(session.execute(q))
        for i, rec in enumerate(patients):
            el = hosp.out(rec.id)
            reply = hosp.post(el)
            try:
                hosp.out_response(reply)
            except OperationException, e:
                logging.getLogger('hexporter').warning('OperationException "%s" was suppressed' % e.message)
                rejected += 1
            else:
                accepted += 1
            yield UpdateProgress(i, len(patients) - 1, _('Doing out'))

        yield MessageBox('Accepted: %s\nRejected: %s' % (accepted, rejected))
