# -*- coding: utf-8 -*-

import datetime
import six
from camelot.core.utils import ugettext, ugettext_lazy as _
from camelot.view.art import Icon
from camelot.admin.action.list_action import (ListContextAction, SaveExportMapping, RestoreExportMapping,
                                              RemoveExportMapping, ClearMapping)

from openpyxl import Workbook
from openpyxl.styles import Font, Border, Side, NamedStyle, PatternFill
from openpyxl.utils import get_column_letter
from utils import log_exceptions


class ExportSpreadsheet(ListContextAction):
    """Export all rows in a table to a spreadsheet"""

    icon = Icon('tango/16x16/mimetypes/x-office-spreadsheet.png')
    tooltip = _('Export to MS Excel')
    verbose_name = _('Export to MS Excel')

    max_width = 40
    font_name = 'Calibri'

    @log_exceptions
    def model_run(self, model_context):
        from decimal import Decimal
        from camelot.view.import_utils import (ColumnMapping,
                                               ColumnSelectionAdmin)
        from camelot.view.utils import (local_date_format,
                                        local_datetime_format,
                                        local_time_format)
        from camelot.view import action_steps
        #
        # Select the columns that need to be exported
        #
        yield action_steps.UpdateProgress(text=_('Prepare export'))
        admin = model_context.admin
        settings = admin.get_settings()
        settings.beginGroup('export_spreadsheet')
        all_fields = admin.get_all_fields_and_attributes()
        field_choices = [(f, six.text_type(entity_fa['name'])) for f, entity_fa in
                         six.iteritems(all_fields)]
        field_choices.sort(key=lambda field_tuple: field_tuple[1])
        row_data = [None] * len(all_fields)
        column_range = six.moves.range(len(all_fields))
        mappings = []
        for i, default_field in six.moves.zip_longest(column_range,
                                                      admin.get_export_columns(),
                                                      fillvalue=(None, None)):
            mappings.append(ColumnMapping(i, [row_data], default_field[0]))

        mapping_admin = ColumnSelectionAdmin(admin, field_choices=field_choices)
        mapping_admin.related_toolbar_actions = [SaveExportMapping(settings),
                                                 RestoreExportMapping(settings),
                                                 RemoveExportMapping(settings),
                                                 ClearMapping(),
                                                 ]
        change_mappings = action_steps.ChangeObjects(mappings, mapping_admin)
        change_mappings.title = _('Select field')
        change_mappings.subtitle = _('Specify for each column the field to export')
        yield change_mappings
        settings.endGroup()
        columns = []
        for i, mapping in enumerate(mappings):
            if mapping.field is not None:
                columns.append((mapping.field, all_fields[mapping.field]))
        #
        # setup worksheet
        #
        yield action_steps.UpdateProgress(text=_('Create worksheet'))
        workbook = Workbook()
        # Workbook contains by default an sheet
        worksheet = workbook.active
        worksheet.title = 'Sheet1'

        #
        # write styles
        #
        title_style = NamedStyle(name='title_style',
                                 font=Font(name=self.font_name,
                                           bold=True,
                                           size=12)
                                 )

        header_style = NamedStyle(name='header_style',
                                  font=Font(bold=True,
                                            name=self.font_name,
                                            color='FFFFFF',
                                            size=10),
                                  fill=PatternFill(fill_type='solid',
                                                   start_color='4F81BD',
                                                   end_color='4F81BD'),
                                  border=Border(top=Side(style='thin',
                                                         color='95B3D7'),
                                                bottom=Side(style='thin',
                                                            color='95B3D7'))
                                  )

        table_fill_odd = NamedStyle(name='table_fill_odd',
                                    font=Font(name=self.font_name,
                                              size=10),
                                    fill=PatternFill(fill_type='solid',
                                                     start_color='C6D9F0',
                                                     end_color='C6D9F0')
                                    )

        table_fill_even = NamedStyle(name='table_fill_even',
                                     font=Font(name=self.font_name,
                                               size=10),
                                     fill=PatternFill(fill_type='solid',
                                                      start_color='FFFFFF',
                                                      end_color='FFFFFF')
                                     )

        border_bottom = Border(bottom=Side(style='thin', color='95B3D7'))
        border_left = Border(bottom=border_bottom.bottom, left=Side(style='thin', color='95B3D7'))
        border_right = Border(bottom=border_bottom.bottom, right=Side(style='thin', color='95B3D7'))

        #worksheet.cell(row=1, column=1).value = admin.get_verbose_name_plural()
        #worksheet.cell(row=1, column=1).style = title_style

        #
        # create some patterns and formats
        #
        date_format = local_date_format()
        datetime_format = local_datetime_format()
        time_format = local_time_format()

        #
        # write headers
        #
        worksheet.auto_filter.ref = 'A1:' + get_column_letter(len(columns)) + '1'
        field_names = []
        for i, (name, field_attributes) in enumerate(columns):
            verbose_name = six.text_type(field_attributes.get('name', name))
            field_names.append(name)
            name = six.text_type(name)

            if i == 0:
                header_style.border = border_left
            elif i == len(columns) - 1:
                header_style.border = border_right
            else:
                header_style.border = border_bottom
            worksheet.cell(row=1, column=i + 1).value = verbose_name
            worksheet.cell(row=1, column=i + 1).style = header_style

            if len(name) < 8:
                worksheet.column_dimensions[get_column_letter(i + 1)].width = 8 * 1.3
            else:
                worksheet.column_dimensions[get_column_letter(i + 1)].width = len(verbose_name) * 1.3

        worksheet.auto_filter.add_filter_column(0, [])

        #
        # write data
        #
        offset = 2
        static_attributes = list(admin.get_static_field_attributes(field_names))
        for j, obj in enumerate(model_context.get_collection(yield_per=100)):
            dynamic_attributes = admin.get_dynamic_field_attributes(obj,
                                                                    field_names)
            row = offset + j
            if j % 100 == 0:
                yield action_steps.UpdateProgress(j, model_context.collection_count)
            fields = enumerate(six.moves.zip(field_names,
                                             static_attributes,
                                             dynamic_attributes))
            for i, (name, attributes, delta_attributes) in fields:
                attributes.update(delta_attributes)
                value = getattr(obj, name)
                format_string = 'General'
                if value is not None:
                    if isinstance(value, Decimal):
                        format_string = '0.00'
                    elif isinstance(value, list):
                        separator = attributes.get('separator', u', ')
                        value = separator.join([six.text_type(el) for el in value])
                    elif isinstance(value, float):
                        precision = attributes.get('precision', 2)
                        format_string = '0.' + '0' * precision
                    elif isinstance(value, int):
                        format_string = '0'
                    elif isinstance(value, datetime.date):
                        format_string = date_format
                    elif isinstance(value, datetime.datetime):
                        format_string = datetime_format
                    elif isinstance(value, datetime.time):
                        format_string = time_format
                    elif attributes.get('to_string') is not None:
                        value = six.text_type(attributes['to_string'](value))
                    else:
                        value = six.text_type(value)
                else:
                    # empty cells should be filled as well, to get the
                    # borders right
                    value = ''

                worksheet.cell(row=row, column=i + 1).value = value

                if (row % 2 == 0):
                    if i == 0:
                        table_fill_even.border = border_left
                    elif i == len(columns) - 1:
                        table_fill_even.border = border_right
                    else:
                        table_fill_even.border = border_bottom
                    worksheet.cell(row=row, column=i + 1).style = table_fill_even
                else:
                    if i == 0:
                        table_fill_odd.border = border_left
                    elif i == len(columns) - 1:
                        table_fill_odd.border = border_right
                    else:
                        table_fill_odd.border = border_bottom
                    worksheet.cell(row=row, column=i + 1).style = table_fill_odd

                min_width = len(six.text_type(value)) * 1
                worksheet.column_dimensions[get_column_letter(i + 1)].width = min(self.max_width, max(
                    min_width,
                    worksheet.column_dimensions[get_column_letter(i + 1)].width)
                                                                                  )
                # number_format must be set at the end, otherwise it will not be applied
                worksheet.cell(row=row, column=i + 1).number_format = format_string

        yield action_steps.UpdateProgress(text=_('Saving file'))
        filename = action_steps.OpenFile.create_temporary_file('.xlsx')
        workbook.save(filename)
        yield action_steps.UpdateProgress(text=_('Opening file'))
        yield action_steps.OpenFile(filename)
