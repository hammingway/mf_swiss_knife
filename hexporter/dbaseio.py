from camelot.admin.action import Action
from camelot.core.utils import ugettext_lazy as _
from camelot.view.art import Icon
from model import Check, TempCheck, Passport, TempPassport, MedicalCare, TempMedicalCare, Notification, TempNotification
from io import BytesIO
from datetime import datetime
from utils import RecordProxy, log_exceptions
import os
import re
import logging

EFER_NAME_INNER = re.compile('(?P<T>\w)'
                             '(?P<LL>\w{2})'
                             '(?P<P>\w)'
                             '(?P<M>[1-9a-c])'
                             '(?P<GG>\d{2})'
                             '(?P<N>[0-9a-f])'
                             '.dbf'
                             , flags=re.I)
EFER_NAME_OUTER = re.compile('(?P<T>\w)'
                             '(?P<LL>\w{2})'
                             '(?P<P>\w)'
                             '(?P<M>[1-9a-c])'
                             '(?P<GG>\d{2})'
                             '(?P<N>[0-9a-f])'
                             '.(?P<SSS>\w{3})'
                             , flags=re.I)
ERRC_CSET = set('1234567890.')


class ImportDBF(Action):
    icon = Icon('tango/16x16/actions/document-open.png')
    verbose_name = _('Open Data')

    @log_exceptions
    def model_run(self, model_context):
        from camelot.view.action_steps import (SelectFile,
                                               UpdateProgress,
                                               Refresh,
                                               FlushSession)

        select_archives = SelectFile('All Files (*)')
        select_archives.single = False
        file_names = yield select_archives
        # file_count = len( file_names )
        # dir_name = os.path.dirname(file_names[0])

        from zipfile import is_zipfile, ZipFile, BadZipfile
        from glob import glob
        from dbfpy import dbf
        from sqlalchemy import orm
        from camelot.core.orm import Session
        from camelot.core.sql import metadata
        from camelot.core.conf import settings
        from camelot.view.action_steps import MessageBox
        from sqlalchemy.sql import delete
        from sqlalchemy.exc import OperationalError
        from dbasetemp import process_temporaries

        def norm_errcode(c):
            f = lambda i: i if i in ERRC_CSET else ''
            return ''.join(map(f, c)).strip('.')

        def read_table(arc, name):

            def arc2mem(arc, name, mem):
                with arc.open(name) as stream:
                    while True:
                        d = stream.read(4096)
                        if not d:
                            break
                        mem.write(d)

            with BytesIO() as memf:
                arc2mem(arc, name, memf)
                tbl = dbf.Dbf(memf, readOnly=True)
                try:
                    for r in tbl:
                        if not r.deleted:
                            yield RecordProxy(r, tbl)
                finally:
                    tbl.close()

        def clean_tables(session):
            lst = (delete(Check.table),
                   delete(TempCheck.table),
                   delete(Passport.table),
                   delete(TempPassport.table),
                   delete(MedicalCare.table),
                   delete(TempMedicalCare.table),
                   delete(Notification.table),
                   delete(TempNotification.table))
            for i in lst:
                session.execute(i)

        def check_load(table, fdate, smo, ptype):
            nomch = u''
            lpu = u''
            pdate = None
            for r in table:
                b = TempCheck()
                r.fill_model(b)
                lpu = r.LPU_K1.upper()
                b.lpu_k1 = lpu
                b.lpu_k2 = r.LPU_K2.upper()
                b.lpu_name = r.LPU_NAME.upper()
                b.smo_name = r.SMO_NAME.upper()
                b.vid_med = r.VID_MED.lower()
                nomch = r.NOM_CH
                b.nom_ch = nomch
                b.ed_izm = r.ED_IZM.lower()
                pdate = r.DATE_PER
                b.date_per = pdate
                b.fail_ch = r.FAIL_CH.lower()
                tdate = datetime(b.date_ch.year, b.date_ch.month, b.date_ch.day)
                if fdate <= tdate:
                    logging.getLogger('hexporter').warning(
                        u'Wrong modification date for check %s (modification: %s, in check: %s)'
                        % (nomch, fdate, b.date_ch))
                    fdate = tdate
                b.date_fil = fdate
                b.smo_kode = smo
                b.type = ptype
            return lpu, nomch, fdate, pdate

        def passport_load(table, fdate, pdate, smo, nomch, ptype):
            for r in table:
                b = TempPassport()
                r.fill_model(b)
                b.nom_ch = nomch
                b.date_per = pdate
                b.date_fil = fdate
                b.smo_kode = smo
                b.type = ptype

        def care_load(table, lpu, fdate, pdate, smo, nomch, ptype):
            for r in table:
                b = TempMedicalCare()
                r.fill_model(b)
                b.vmedpom = r.VMEDPOM.lower()
                b.kod_usl = r.KOD_USL.lower()
                b.kod_oper = r.KOD_OPER.lower()
                b.lpu = lpu
                b.nom_ch = nomch
                b.date_per = pdate
                b.date_fil = fdate
                b.smo_kode = smo
                b.type = ptype

        def notification_load(table, nomch, fdate, pdate, ptype):
            for r in table:
                b = TempNotification()
                r.fill_model(b)
                b.vmedpom = r.VMEDPOM.lower()
                b.err_code = norm_errcode(r.ERR_CODE)
                b.ncard = r.NCARD.lower()
                b.smo = str(r.SMO)
                b.lpu = r.LPU.upper()
                b.rstamp = r.RSTAMP.lower()
                b.nom_ch = nomch
                b.date_per = pdate
                b.date_fil = fdate
                b.type = ptype

        def name_of_types(names, tps):

            def is_name_of_types(name, tps):
                mg = EFER_NAME_INNER.match(name)
                if mg is None:
                    return False
                return mg.group('T') in tps

            return next((nm for nm in names if is_name_of_types(nm, tps)), None)

        def match_file(fname):
            return EFER_NAME_OUTER.match(os.path.basename(fname))

        # session = Session()
        session = model_context.session
        with session.begin():
            clean_tables(session)

            file_names = filter(match_file, file_names)
            file_count = len(file_names)
            logging.getLogger('hexporter').info(u'Import from | %s' % ' '.join(map(os.path.basename, file_names)))
            for i, file_name in enumerate(file_names):
                yield UpdateProgress(i, file_count)
                mg = match_file(file_name)
                smo = unicode(mg.group('SSS').lower())
                ptype = unicode(mg.group('T').lower())
                fdate = datetime.fromtimestamp(os.stat(file_name).st_mtime)
                try:
                    with ZipFile(file_name, 'r') as arc:
                        afnames = arc.namelist()
                        if ptype in {u'3', u'4', u'5'}:
                            # read check and get its number
                            name = name_of_types(afnames, {'6'})
                            if name is None:
                                continue
                            lpu, nom_ch, cdate, pdate = check_load(read_table(arc, name), fdate, smo, ptype)
                            # read notification
                            name = name_of_types(afnames, {'4', '5'})
                            if name is not None:
                                notification_load(read_table(arc, name), nom_ch, cdate, pdate, ptype)
                            yield FlushSession(session)
                        elif ptype in {u'6'}:
                            # read check and get its number
                            name = name_of_types(afnames, {'6', '8'})
                            if name is None:
                                continue
                            lpu, nom_ch, cdate, pdate = check_load(read_table(arc, name), fdate, smo, ptype)
                            # read passports
                            name = name_of_types(afnames, {'1'})
                            if name is not None:
                                passport_load(read_table(arc, name), cdate, pdate, smo, nom_ch, ptype)
                            # read cares
                            name = name_of_types(afnames, {'2'})
                            if name is not None:
                                care_load(read_table(arc, name), lpu, cdate, pdate, smo, nom_ch, ptype)
                            yield FlushSession(session)
                except BadZipfile:
                    pass
                except Exception, e:
                    logging.getLogger('hexporter').warning(
                        u'Exception during reading of %s | ' % os.path.basename(file_name),
                        exc_info=True)
                    # yield MessageBox(u'Exception during reading of %s:\n\n%s' % (os.path.basename(file_name), repr(e)),
                    #                  QtGui.QMessageBox.Critical)

            process_temporaries(session)
            yield FlushSession(session)
        yield Refresh()
