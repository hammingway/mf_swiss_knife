# -*- coding: utf-8 -*-

import os
import logging
import six
from camelot.view.art import Icon
from camelot.view.action_steps import OpenFile, MessageBox
from camelot.admin.application_admin import ApplicationAdmin
from camelot.admin.action import Action, application_action, list_action
from camelot.admin.section import Section
from camelot.core.utils import ugettext, ugettext_lazy as _
from camelot.core.qt import Qt, QtCore, QtWidgets
from model import (Check, Passport, MedicalCare, MedFoundation, Statkart, Hospitalized, DisprEntity, DisprsltEntity,
                   S007Entity)
from view import Completed, Errors, Period
from dbaseio import ImportDBF
from hospio import HospitalOut
from constant import UpdateFixtures
from exporter import ExportSpreadsheet


class MyApplicationAdmin(ApplicationAdmin):
    name = 'MF Swiss Knife'
    author = 'PFP'
    application_url = 'https://bitbucket.org/hammingway/mf_swiss_knife/'
    domain = 'local'

    def get_sections(self):
        from camelot.model.memento import Memento
        from camelot.model.i18n import Translation

        return [Section(_('Health examination'),
                        self,
                        Icon('icons/22x22/stethoscope_22.png'),
                        items=[Section(_('Reports'),
                                       self,
                                       Icon('tango/16x16/categories/applications-system.png'),
                                       items=[Completed],
                                       ),
                               Section(_('Selfcheck'),
                                       self,
                                       Icon('tango/16x16/emblems/emblem-important.png'),
                                       items=[Errors, Period]),
                               Section(_('Source data'),
                                       self,
                                       Icon('tango/16x16/apps/system-users.png'),
                                       items=[Check, Passport, MedicalCare]),
                               Section(_('References'),
                                       self,
                                       Icon('tango/16x16/apps/accessories-text-editor.png'),
                                       items=[DisprsltEntity, S007Entity, MedFoundation])
                               ],
                        ),
                Section(_('Hospitalization'),
                        self,
                        Icon('icons/22x22/hospital_22.png'),
                        items=[Section(_('References'),
                                       self,
                                       Icon('tango/16x16/apps/accessories-text-editor.png'),
                                       items=[Statkart, Hospitalized])],
                        ),
                Section(_('Configuration'),
                        self,
                        Icon('tango/22x22/categories/preferences-system.png'),
                        items=[Memento, Translation])
                ]

    def get_main_menu(self):
        from camelot.admin.menu import Menu

        return [Menu(_('&File'),
                     [application_action.Backup(),
                      application_action.Restore(),
                      None,
                      Menu(_('Export To'),
                           [ExportSpreadsheet()]),
                      Menu(_('Import From'),
                           [list_action.ImportFromFile()]),
                      None,
                      Menu(_('Health examination'),
                           [ImportDBF()]),
                      None,
                      Menu(_('Hospitalization'),
                           [HospitalOut()]),
                      None,
                      application_action.Exit(),
                      ]),
                Menu(_('&Edit'),
                     self.edit_actions + [
                         None,
                         list_action.SelectAll(),
                         None,
                         list_action.ReplaceFieldContents(),
                     ]),
                Menu(_('View'),
                     [application_action.Refresh(),
                      Menu(_('Go To'), self.change_row_actions)]),
                # Menu(_('Administration'),
                #      [UpdateFixtures()]),
                Menu(_('&Help'),
                     [
                         ShowAbout(self.application_url),
                         None,
                         OpenLog()])
                ]

    def get_toolbar_actions(self, toolbar_area):
        if toolbar_area == Qt.TopToolBarArea:
            return [ExportSpreadsheet()]

    def get_related_toolbar_actions(self, toolbar_area, direction):
        if toolbar_area == Qt.RightToolBarArea and direction == 'onetomany':
            return [ExportSpreadsheet()]
        if toolbar_area == Qt.RightToolBarArea and direction == 'manytomany':
            return [ExportSpreadsheet()]

    def get_translator(self):
        translators = super(MyApplicationAdmin, self).get_translator()
        locale_name = QtCore.QLocale().name()
        my_translator = self._load_translator_from_file('hexporter',
                                                        os.path.join('%s/LC_MESSAGES/' % locale_name, 'messages'),
                                                        'translations/')
        if my_translator:
            translators.append(my_translator)
        else:
            logging.getLogger('hexporter').debug('no application translations found for %s' % locale_name)
        return translators


class ShowAbout(Action):
    """Show the about dialog with the content returned by the
    :meth:`ApplicationAdmin.get_about` method
    """

    verbose_name = _('&About')
    icon = Icon('tango/16x16/mimetypes/application-certificate.png')
    tooltip = _("Show the application's About box")

    def __init__(self, application_url):
        super(ShowAbout, self).__init__()
        self.application_url = application_url

    def gui_run(self, gui_context):
        def get_about():
            return unicode(_("<b>HE Exporter</b><br/>\n"
                             "this software built on top of the Camelot framework\n"
                             "<p>\n"
                             "Do any feedback via <a href='mailto:%(mail)s'>\n"
                             "%(mail)s\n"
                             "</a>")) % {"mail": self.application_url}

        abtmsg = get_about()
        QtWidgets.QMessageBox.about(gui_context.workspace,
                                    ugettext('About'),
                                    six.text_type(abtmsg))


class OpenTextStream(OpenFile):
    def __init__(self, stream, suffix='.txt'):
        import os
        import tempfile

        file_descriptor, file_name = tempfile.mkstemp(suffix=suffix)
        output_stream = os.fdopen(file_descriptor, 'w')
        output_stream.writelines(('%s' + os.linesep) % l.rstrip('\n') for l in stream.readlines())
        output_stream.close()
        super(OpenTextStream, self).__init__(file_name)


class OpenLog(Action):
    verbose_name = _('Open log')

    def model_run(self, model_context):
        from camelot.core.qt import QtGui
        import os

        try:
            with open(os.path.expanduser('~/%s' % 'hexporter.log'), 'r') as l:
                yield OpenTextStream(l)
        except Exception, e:
            yield MessageBox(_('Can\'t open logfile!'), QtGui.QMessageBox.Critical)
