# -*- coding: utf-8 -*-

import types
import traceback
import logging
import sys
import re
from functools import wraps
from dbfpy import dbf
from camelot.core.exception import CancelRequest


class Translator(object):
    AT_START = 1
    AT_END = 2

    def __init__(self, dlist, mode=None):
        self.rep = {re.escape(k): v for k, v in dlist}
        if mode == self.AT_START:
            self.pattern = re.compile("|".join(ur'\A%s' % k for k in self.rep.keys()), re.U)
        elif mode == self.AT_END:
            self.pattern = re.compile("|".join(u'%s$' % k for k in self.rep.keys()), re.U)
        else:
            self.pattern = re.compile("|".join(self.rep.keys()), re.U)

    def translate(self, text):
        return self.pattern.sub(lambda m: self.rep[re.escape(m.group(0))], text)

    def replace(self, text, string=u''):
        return self.pattern.sub(string, text)

    def suggest(self, text):
        m = self.pattern.search(text)
        if m is not None:
            return m.group(0), self.rep[m.group(0)]
        return None


_RUS_REPL = ((u'е', 'e'), (u'т', 't'), (u'о', 'o'), (u'р', 'p'), (u'а', 'a'), (u'н', 'h'), (u'к', 'k'), (u'х', 'x'),
             (u'с', 'c'), (u'в', 'b'), (u'м', 'm'), (u'Е', 'E'), (u'Т', 'T'), (u'О', 'O'), (u'Р', 'P'), (u'А', 'A'),
             (u'Н', 'H'), (u'К', 'K'), (u'Х', 'X'), (u'С', 'C'), (u'В', 'B'), (u'М', 'M'))

RUS_REPL = Translator(_RUS_REPL)
ENG_REPL = Translator((r, l) for l, r in _RUS_REPL)


def mk_rus(s):
    return ENG_REPL.translate(s)


def mk_eng(s):
    return RUS_REPL.translate(s)


def fields_from_list(obj, attrs):
    return [getattr(obj.c, i) for i in attrs]


ENCODING = {0x26: 'cp866', 0x65: 'cp866', 0x96: 'mac_cyrillic', 0xC9: 'cp1251'}  # 0x65 - cp866


class RecordProxy(object):
    def __init__(self, r, tbl):
        self.table__ = tbl
        self.record__ = r
        self.encoding__ = ENCODING.get(tbl.header.languageDriver, 'cp866')

    def __getattr__(self, item):
        v = self.record__[item]
        if isinstance(v, (str, unicode)):
            return v.strip().decode(self.encoding__)
        return v

    def fill_model(self, model):
        for n in self.table__.fieldNames:
            attrname = n.lower()
            if hasattr(model, attrname):
                setattr(model, attrname, self.__getattr__(n))


def read_dbf(name):
    tbl = dbf.Dbf(name, readOnly=True)
    try:
        for r in tbl:
            if not r.deleted:
                yield RecordProxy(r, tbl)
    finally:
        tbl.close()


_NOTEXCEPTIONS = (StopIteration, GeneratorExit, KeyboardInterrupt, SystemExit, CancelRequest)


def hide_exceptions(*exceptions):
    def deco(func):
        def generators_wrapper(tested_generator):
            val = None
            while True:
                try:
                    val = yield tested_generator.send(val)
                except exceptions:
                    pass

        @wraps(func)
        def tested_function(*args, **kwargs):
            fresult = None
            try:
                fresult = func(*args, **kwargs)
            except exceptions:
                pass
            if isinstance(fresult, types.GeneratorType):
                return generators_wrapper(fresult)
            return fresult

        return tested_function
    return deco


def log_exceptions(func):
    def generators_wrapper(tested_generator):
        val = None
        while True:
            try:
                val = yield tested_generator.send(val)
            except BaseException as e:
                if not isinstance(e, _NOTEXCEPTIONS):
                    etype, value, tb = sys.exc_info()
                    logging.getLogger('hexporter').error(''.join(traceback.format_exception(etype, value, tb.tb_next)))
                raise e

    @wraps(func)
    def tested_function(*args, **kwargs):
        try:
            fresult = func(*args, **kwargs)
        except BaseException as e:
            if not isinstance(e, _NOTEXCEPTIONS):
                etype, value, tb = sys.exc_info()
                logging.getLogger('hexporter').error(''.join(traceback.format_exception(etype, value, tb.tb_next)))
            raise e
        if isinstance(fresult, types.GeneratorType):
            return generators_wrapper(fresult)
        return fresult
    return tested_function
