# -*- coding: utf-8 -*-

import re
import sys
import logging
from collections import namedtuple, OrderedDict
from utils import mk_eng, mk_rus

_text = u'''
DispR       IDDR        О   N(2)    Код результата диспансеризации
            DRNAME      О   T(254)  Наименование результата диспансеризации
            DATEBEG     О   D       Дата начала действия записи
            DATEEND     У   D       Дата окончания действия записи
DispRslt    IDDR        О   N(2)    Код результата диспансеризации
            DRNAME      О   T(254)  Наименование результата диспансеризации
            DATEBEG     О   D       Дата начала действия записи
            DATEEND     У   D       Дата окончания действия записи
            RSLT        У   N(3)
S007        n_mp        О   N(8)
            vmedpom     O   T(1)
            name_mp     O   T(254)
            d_begin     O   D
'''

TableSpec = namedtuple('TableSpec', ['to_one', 'fields', 'attribs'])
SpecVField = namedtuple('SpecVField', ['fld', 'spc', 'typ'])
SpecVType = namedtuple('SpecVType', ['id', 'size', 'prec'])


class DataValidityException(Exception):
    pass


def get_type(s):
    fs = s.split('(')
    if len(fs) > 1:
        t, r = fs
    else:
        t, r = fs[0], ''
    if r:
        ss = map(int, r.rstrip(')').split('.'))
    else:
        ss = [0]
    return SpecVType(mk_eng(t), ss[0], ss[1] if len(ss) > 1 else 0)


def make_specs(text):
    group = ''
    specs = OrderedDict()
    for line in re.sub('[ \t\f\v]+', ' ', text).strip().split('\n'):
        grp, fld, spc, typ = line.split(' ')[:4]
        grp = mk_eng(grp)
        if grp:
            group = grp
        else:
            grp = group
        fld = mk_eng(fld)
        spc = mk_rus(spc)
        typ = get_type(typ)
        specs.setdefault(grp, TableSpec([], [], {'is_node': False})).fields.append(SpecVField(fld, spc, typ))
    for grp, val in specs.items():
        for fld in val.fields:
            if fld.typ.id == 'S':
                specs[fld.fld].to_one.append(grp)
                specs[grp].attribs['is_node'] = True
    return specs


specs_all = make_specs(_text)


def table_name(name):
    return 'entity_%s' % name.lower()


def entity_name(name):
    return str('%sEntity' % name.title())


def entity_by_spec(name, specs, collation=None):
    from camelot.admin.entity_admin import EntityAdmin
    from sqlalchemy.schema import Column
    from sqlalchemy import Unicode, Date, Numeric, BigInteger
    from camelot.core.orm import Entity, ManyToOne

    def gen_fields(specs):
        for fld, spc, (id_, size, prec) in specs:
            kwargs = {}
            if fld.lower() == 'id':
                kwargs['primary_key'] = True
            if id_ == 'T':
                if collation is None:
                    col = Column(fld, Unicode(size), **kwargs)
                else:
                    col = Column(fld, Unicode(size, collation=collation), **kwargs)
            elif id_ == 'N':
                if prec:
                    col = Column(fld, Numeric(precision=size, scale=prec), **kwargs)
                else:
                    col = Column(fld, BigInteger, **kwargs)
            elif id_ == 'D':
                col = Column(fld, Date, **kwargs)
            elif id_ == 'S':
                continue
            else:
                raise DataValidityException('unknown type specification "%s" at field "%s"' % (id_, fld))
            yield fld, spc, col

    columns = OrderedDict((f, c) for f, _, c in gen_fields(specs.fields))
    the_dict = {'__tablename__': table_name(name),
                'Admin': type('Admin',
                              (EntityAdmin,),
                              {'verbose_name_plural': name,
                               'list_display': columns.keys()})}
    the_dict.update(columns)
    for grp in specs.to_one:
        the_dict[grp.lower()] = ManyToOne(entity_name(grp), required=False)
    return type(entity_name(name), (Entity,), the_dict)


def dbf_by_spec(filename, name, specs, dataset, translators=None):
    from dbfpy import dbf
    from operator import itemgetter

    def create_format(specs):

        def get_spec(id_, size, prec):
            if id_ == 'N':
                return ['N', size, prec]
            elif id_ == 'T':
                return ['C', size]
            elif id_ == 'D':
                return ['D']

        return ([fld] + get_spec(id_, size, prec) for fld, spc, id_, size, prec in specs)

    def validate(val, fld):
        if fld in imp_fields and val is None:
            raise DataValidityException('None value for field [%s]' % fld)
        return val

    def encode(val, codepage='cp866'):
        if isinstance(val, unicode):
            return val.encode(codepage)
        return val

    if translators is None:
        translators = {}
    fields = [
        (fld, spc, id_, size, prec)
        for fld, spc, (id_, size, prec) in specs[name].fields
        if id_ != 'S' and u'М' not in spc]
    used_fields = set(map(itemgetter(0), fields))
    imp_fields = set(fld for fld, spc, id_, size, prec in fields if u'О' in spc)
    tbl = dbf.Dbf(filename, new=True, languageDriver=0x65)
    tbl.addField(create_format(fields))
    try:
        for rec in dataset:
            nrec = tbl.newRecord()
            for fld, val in rec.iteritems():
                if fld in used_fields:
                    nrec[fld] = encode(validate(translators.get(fld, lambda x: x)(val), fld))
            nrec.store()
    finally:
        tbl.close()


def from_dbf_by_spec(filename, session, name, translators=None, defaults=None):
    from dbfpy import dbf
    from camelot.view.action_steps import FlushSession
    from utils import ENCODING
    import model

    cls = model.autogenerated[entity_name(name)]
    tbl = dbf.Dbf(filename, readOnly=True)
    encoding = ENCODING.get(tbl.header.languageDriver, 'cp866')
    try:
        specs = {
            i.fld.lower(): i for i in model.specs_all[name].fields
            if i.typ.id != 'S' and u'М' not in i.spc}
        ofields = set(tbl.fieldNames)
        ifields = set(specs.keys())
        mfields = set(i.fld.lower() for i in model.specs_all[name].fields if u'О' in i.spc)
        outf = ifields ^ ofields
        absentf = mfields & outf
        if outf:
            logging.getLogger('hexporter').warning('fields out of format [%s]' % ', '.join(sorted(outf)))
        if absentf:
            raise DataValidityException('Expected presence of fields [%s]' % ', '.join(sorted(absentf)))
        for i, r in enumerate(tbl):
            if r.deleted:
                continue
            d = cls()
            for n, v in zip(tbl.fieldNames, r):
                if n not in ifields:
                    continue
                if isinstance(v, (str, unicode)):
                    v = v.strip().decode(encoding)
                sp = specs[n]
                if v and sp.typ.id == 'T':
                    if not v:
                        val = None
                    else:
                        val = v
                else:
                    val = v
                if translators and sp.fld in translators:
                    val = translators[sp.fld](val)
                if val is None and u'О' in sp.spc:
                    raise DataValidityException('None value for field [%s]' % sp.fld)
                setattr(d, sp.fld, val)
            if defaults:
                for k, v in defaults.iteritems():
                    setattr(d, k, v)
            if i % 200 == 0:
                yield FlushSession(session)
        yield FlushSession(session)
    finally:
        tbl.close()


def iter_specs(specs, collation=None):
    for n, v in specs.iteritems():
        yield entity_name(n), entity_by_spec(n, v, collation)


def get_spec_val(tbl, fld):
    return filter(lambda x: x.fld == fld, specs_all[tbl].fields)[0]


if __name__ == '__main__':
    entity_by_spec('Medspec', specs_all['Medspec'])
