#
# Default setup file for a Camelot application
#
# To build a windows installer, execute this file with :
#
# python setup.py egg_info bdist_cloud wininst_cloud
#
# Running from the Python SDK command line
#

import datetime

from setuptools import setup, find_packages

setup(
    name='My Application',
    version='1.0',
    author='My Company',
    url='http://www.python-camelot.com',
    include_package_data=True,
    packages=find_packages(),
    py_modules=['settings', 'main'],
    entry_points={'gui_scripts': [
        'main = main:start_application',
    ], },
    options={
        'bdist_cloud': {'revision': '0',
                        'branch': 'master',
                        'uuid': 'eb110874-4d01-471e-8463-70305966537c',
                        'update_before_launch': False,
                        'default_entry_point': ('gui_scripts', 'main'),
                        'changes': [],
                        'timestamp': datetime.datetime.now(),
        },
        'wininst_cloud': {'excludes': 'excludes.txt',
                          'uuid': 'eb110874-4d01-471e-8463-70305966537c', },
    },

)

    