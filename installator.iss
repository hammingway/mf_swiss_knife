﻿[Setup]
OutputBaseFilename="MF Swiss Knife mini"
OutputDir=.
AppId={{D9A8248C-F76F-48C9-9A7C-66AFB6C309F9}
AppName="MF Swiss Knife mini"
AppVersion=2.0.0
VersionInfoVersion=2.0.0.0
VersionInfoDescription=MF Swiss Knife mini setup
VersionInfoProductName=MF Swiss Knife mini
AppendDefaultDirName=no
DefaultDirName="{pf}\{cm:Name}"
DefaultGroupName="{cm:Name}"
UninstallFilesDir={pf}\{cm:Name}\uninstall

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"

[CustomMessages]
en.Name=MF Swiss Knife mini
ru.Name=МО Универсал мини
en.UninstIcon=Uninstall
ru.UninstIcon=Деинсталлировать
en.Launch=MF Swiss Knife mini
ru.Launch=МО Универсал мини

[Files]
Source: "Python27CamelotNoShared_sqlite\*"; Excludes: "*.pyc,*.pyo"; DestDir: "{app}\Python27CamelotNoShared_sqlite"; Flags: recursesubdirs
Source: "hexporter\*"; Excludes: "*.pyc,*.pyo"; DestDir: "{app}\hexporter"; Flags: recursesubdirs
Source: "dbfpy\*"; Excludes: "*.pyc,*.pyo"; DestDir: "{app}\dbfpy"; Flags: recursesubdirs
Source: "data\*"; DestDir: "{app}\data"; Flags: recursesubdirs
Source: "__init__.py"; DestDir: "{app}"
Source: "main.py"; DestDir: "{app}"
Source: "setup.py"; DestDir: "{app}"
Source: "system-users-3.ico"; DestDir: "{app}"

Source: "install.cmd"; DestDir: "{tmp}"
Source: "vcredist_x86.exe"; DestDir: "{tmp}"

[Icons]
Name: "{group}\{cm:Launch}"; Filename: "{app}\Python27CamelotNoShared_sqlite\pythonw.exe"; Parameters: "main.py"; WorkingDir: "{app}"; IconFilename: "{app}\system-users-3.ico"
Name: "{commondesktop}\{cm:Launch}"; Filename: "{app}\Python27CamelotNoShared_sqlite\pythonw.exe"; Parameters: "main.py"; WorkingDir: "{app}"; IconFilename: "{app}\system-users-3.ico"
Name: "{group}\{cm:UninstIcon}"; Filename: "{uninstallexe}"

[UninstallDelete]
Name: "{app}\Python27CamelotNoShared_sqlite"; Type: filesandordirs
Name: "{app}\hexporter"; Type: filesandordirs
Name: "{app}\dbfpy"; Type: filesandordirs
Name: "{app}\data"; Type: filesandordirs
Name: "{app}\*.py"; Type: files
Name: "{app}\*.ico"; Type: files

[Code]

const
enUninstConfirm = 'Uninstall is initializing. Do you really want to start Uninstall?';
ruUninstConfirm = 'Инициализация деинсталляции. Вы действительно хотите деинсталлировать?';

function InitializeSetup(): Boolean;
begin
  Result := True;
end;

function GetUninstallString(): String;
var
  sUnInstPath: String;
  sUnInstallString: String;
begin
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{#emit SetupSetting("AppId")}_is1');
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end;
 
function IsUpgrade(): Boolean;
begin
  Result := (GetUninstallString() <> '');
end;
 
function UnInstallOldVersion(): Integer;
var
  sUnInstallString: String;
  iResultCode: Integer;
begin
// Return Values:
// 1 - uninstall string is empty
// 2 - error executing the UnInstallString
// 3 - successfully executed the UnInstallString
 
  // default return value
  Result := 0;
 
  // get the uninstall string of the old app
  sUnInstallString := GetUninstallString();
  if sUnInstallString <> '' then begin
    sUnInstallString := RemoveQuotes(sUnInstallString);
    if Exec(sUnInstallString, '/SILENT /NORESTART /SUPPRESSMSGBOXES','', SW_HIDE, ewWaitUntilTerminated, iResultCode) then
      Result := 3
    else
      Result := 2;
  end else
    Result := 1;
end;

procedure AfterComponentsInstall();
var
  ResultCode: Integer;
begin
  if Exec(ExpandConstant('{tmp}\install.cmd'), '', ExpandConstant('{tmp}'), SW_HIDE, ewWaitUntilTerminated, ResultCode) then
  begin
     // handle success if necessary; ResultCode contains the exit code
  end;
end;
 
procedure CurStepChanged(CurStep: TSetupStep);
begin
  case CurStep of
    ssInstall:
      begin
        if (IsUpgrade()) then
        begin
          UnInstallOldVersion();
        end;
      end;
    ssPostInstall:
      begin
        AfterComponentsInstall();
      end;
  end;
end;

function InitializeUninstall(): Boolean;
var
  Message : string;
begin
  case ActiveLanguage of
    'en' : Message := enUninstConfirm;
    'ru' : Message := ruUninstConfirm;
  end;
  Result := MsgBox(Message, mbConfirmation, MB_YESNO) = idYes;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
//var
//  ResultCode: Integer;
begin
  case CurUninstallStep of
    usUninstall:
      begin
      end;
//    usPostUninstall:
//      begin
        // ...insert code to perform post-uninstall tasks here...
//      end;
  end;
end;
