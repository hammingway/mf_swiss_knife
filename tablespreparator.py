# -*- coding: utf-8 -*-
text = u'''1	СМО	N	7	NPR_E
2	МО	N	7	NPR
3	МО	N	7	NPR_S
4	МО	N	3	FACT_DN
5	МО	N	5.2	KOL_YE
6	МО	C	1	VMEDPOM
				
7	СМО	N	1	REZULTAT
				
				
8	СМО	С	10	ERR_CODE
9	МО	N	5	IDPROF
10	МО	N	16	REGN_PAC
11	МО	N	12.2	SUMMA
12	СМО	N	12.2	SUM_OTKL
13	МО	С	17	NCARD
14	МО	N	3	SMO
15	МО	С	2	LPU
16	МО	N	3	KOL_USL
17	СМО	С	8	RSTAMP
18	СМО	C	1	PRIKREP
19	СМО	C	2	LPU_PRIK
20	МО	C	6	DSO
				
21	МО	D	8	DBEG
22	МО	D	8	DEND
23	МО	C	10	KOD_USL
'''
    
import re

_RUS_REPL = ((u'е', 'e'), (u'т', 't'), (u'о', 'o'), (u'р', 'p'), (u'а', 'a'), (u'н', 'h'), (u'к', 'k'), (u'х', 'x'),
             (u'с', 'c'), (u'в', 'b'), (u'м', 'm'), (u'Е', 'E'), (u'Т', 'T'), (u'О', 'O'), (u'Р', 'P'), (u'А', 'A'),
             (u'Н', 'H'), (u'К', 'K'), (u'Х', 'X'), (u'С', 'C'), (u'В', 'B'), (u'М', 'M'))

class Translator(object):
    AT_START = 1
    AT_END = 2

    def __init__(self, dlist, mode=None):
        self.rep = {re.escape(k): v for k, v in dlist}
        if mode == self.AT_START:
            self.pattern = re.compile("|".join(ur'\A%s' % k for k in self.rep.keys()), re.U)
        elif mode == self.AT_END:
            self.pattern = re.compile("|".join(u'%s$' % k for k in self.rep.keys()), re.U)
        else:
            self.pattern = re.compile("|".join(self.rep.keys()), re.U)

    def translate(self, text):
        return self.pattern.sub(lambda m: self.rep[re.escape(m.group(0))], text)

    def replace(self, text, string=u''):
        return self.pattern.sub(string, text)

    def suggest(self, text):
        m = self.pattern.search(text)
        if m is not None:
            return m.group(0), self.rep[m.group(0)]
        return None

RUS_REPL = Translator(_RUS_REPL)

records_re = re.compile(ur'(\d+)\.?\s+(\w+)\s+(\w)\s+([\d\.]+)\s+(\w+)', re.I + re.U)

def mk_eng(s):
    return RUS_REPL.translate(s)

def spec(c, s):
    c = mk_eng(c.lower())
    if c == 'c':
        return 'Unicode(%s)' % s
    elif c == 'd':
        return 'Date'
    elif c == 'n':
        s = s.split('.')
        if len(s) == 1:
            return 'BigInteger'
        elif len(s) == 2:
            return 'Numeric(precision=%s, scale=%s)' % tuple(s)
        raise Exception('FORMAT')
    raise Exception('FORMAT')

data = list(records_re.findall(text))
for i, v in enumerate(data, 1):
    if i == int(v[0]):
        print '%s = Column(%s)' % (mk_eng(v[-1]).lower(), spec(v[2], v[3]))

print '[%s]' % ', '.join("'%s'" % i[-1].lower() for i in data)

